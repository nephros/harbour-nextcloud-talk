#include <QString>

namespace NCT {
namespace Helpers {
namespace Strings {

    void ltrim(QString *item, QString character) {
        while(item->at(0) == character) {
            item->remove(0, 1);
        }
    }

}}}


